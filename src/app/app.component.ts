import { Component } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private http: HttpClient) {
  }
  ip = 'http://localhost';
  port = '8080';
  title = 'is-registered-web-ui';
  id: string;
  result: string;

  addID(id: string) {
    this.id = id;
  }

  Submit() {
    this.http.get<string>(this.ip + ':' + this.port + '/registerGuest/isRegistered/' + this.id,  {responseType: 'text' as 'json'})
      .subscribe(data => {
          this.result = 'The person is registered';
        },
        (error: HttpErrorResponse) => {
        this.result = 'The person is not registered';
    }
      )
    ;
  }
}
